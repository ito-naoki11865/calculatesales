package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String FILE_ILLEGAL_FORMAT = "のフォーマットが不正です";
	private static final String FILE_ILLEGAL_CODE = "の支店コードが不正です";
	private static final String MONEY_OVER_10DIGITS = "合計金額が10桁を超えました";
	private static final String COMMODITY_FILE_ILLEGAL_CODE = "の商品コードが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "^[0-9]{3}+$")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", "^[0-9A-Za-z]{8}+$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<File>();

		for(int i = 0; i < files.length; i++) {
			String filesName = files[i].getName();
			if((files[i].isFile()) && (filesName.matches("^[0-9]{8}.rcd$"))) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);

		for(int j = 0; j < rcdFiles.size() -1; j++) {
			File rcdFilesNames1 = rcdFiles.get(j);
			String rcdFilesCode1 = rcdFilesNames1.getName().toString();
			File rcdFilesNames2 = rcdFiles.get(j + 1);
			String rcdFilesCode2 = rcdFilesNames2.getName().toString();

			int former = Integer.parseInt(rcdFilesCode1.substring(0,8));
			int latter = Integer.parseInt(rcdFilesCode2.substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}
		for(int i = 0; i < rcdFiles.size(); i++) {

			File rcdFile = rcdFiles.get(i);
			BufferedReader br = null;
			String rcdFilesNames = rcdFile.getName();

			try {
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);
				String s;
				List<String> rcdData = new ArrayList<String>();

				while((s = br.readLine()) != null) {
					rcdData.add(s);
				}
				String branchCode = rcdData.get(0);
				String commodityCode = rcdData.get(1);
				String salesAmount = rcdData.get(2);
				long branchFileSale;
				long commodityFileSale;

				if(rcdData.size() != 3) {
					System.out.println(rcdFilesNames + FILE_ILLEGAL_FORMAT);
					return;
				}
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFilesNames + FILE_ILLEGAL_CODE);
					return;
				}
				if (!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFilesNames + COMMODITY_FILE_ILLEGAL_CODE);
					return;
				}
				if(!salesAmount.matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				branchFileSale = Long.parseLong(salesAmount);
				commodityFileSale = Long.parseLong(salesAmount);
				Long branchSaleAmount = branchSales.get(branchCode) + branchFileSale;
				Long commoditySaleAmount = branchSales.get(branchCode) + commodityFileSale;

				if((branchSaleAmount >= 10000000000L) && (commoditySaleAmount >= 10000000000L)){
					System.out.println(MONEY_OVER_10DIGITS);
					return;
				}
				branchSales.put(branchCode, branchSaleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);
			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}
			finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String errorName, String matches) {

		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(errorName + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				if((items.length != 2) && (!items[0].matches("matches"))){
					System.out.println(errorName + FILE_INVALID_FORMAT);
					return false;
				}
				Names.put(items[0], items[1]);
				Sales.put(items[0] , 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String FileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File file = new File(path, FileName);
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
